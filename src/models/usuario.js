const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
    nombre: String,
    dni: String,
    email: String,
    telefono: String,
    genero: String,
})



// compiling the user schema
var User = mongoose.model('User', userSchema);

// make this available to our users in our Node applications
module.exports = User;