const express = require('express');
const router =  express.Router();
const User =  require('../models/usuario')


router.get('/', (req,res) => { 
    res.render('index.html', {title: 'CV Upload System'});
})

router.get('/iniciosesion', (req,res) => { 
    res.render('iniciosesion.html', {title: 'Inicio de Sesión'});
})

router.get('/muestra', async (req,res) => { 
    let users = await User.find();
    let alert = 0;
    res.render('muestra.html', {
        title: 'Lista en cards',
        users,
        alert
    })
})

router.post('/search', async (req, res) => {
    const {dni} = req.body;
    let buscados = [];
    let users = await User.find();
    let alert = 0;
    console.log(dni)

    for (var i = 0 ; i < users.length; i++) {
        if(users[i].dni == dni){
            buscados.push(users[i])
        }
        
    }
    if(buscados.length > 0){
        users = buscados; 
        res.render('muestra.html', {
            title: 'Lista en cards',
            users,
            alert
        })
    }else{
        if(dni == ""){
            res.redirect('/muestra')
        }
        else{
            users = 0 
            alert = 1
            res.render('muestra.html', {
                title: 'Lista en cards',
                users,
                alert
            })
        }
        
    }
    
    
    
})

router.get('/carga', (req,res) => {
    res.render('carga.html', {title: 'Inicio', fecha: new Date()})
})

router.get('/academica', (req,res) => {
    res.render('academica.html', {title: 'Información Academica'})
})

router.get('/lista', async (req,res) => { 
    const users = await User.find();
    res.render('lista.html', {
        title: 'Lista de Perfiles',
        users
    })
    // console.log(users);
   // res.render('lista.html', {title: 'Lista de Perfiles'});
})

router.get('/cargaperfil', (req,res) => { 
   /* let errors = []
    let nombre, 
    dni,
    email,
    telefono,
    genero;*/
res.render('cargaperfil.html', {title: 'Carga de Perfiles', /*errors, nombre, dni, email, telefono, genero*/ });
})

router.post('/add', async(req, res) => {
    const {nombre, dni, email, telefono, genero} = req.body;
 /*   let errors = [];
    
    if(!nombre){
        errors.push({text : 'Porfavor Ingrese Nombre Completo'});
    }
    if(!dni){
        errors.push({text: 'Ingrese por favor DNI'})
    }
    if(!email){
        errors.push({text: 'Ingrese por favor una Dirección de Email'})
    }
    if(!telefono){
        errors.push({text: 'Ingrese por favor un Telefono'})
    }
    if(!genero){
        errors.push({text: 'Ingrese por favor Genero'})
    }
    if(errors.length > 0 ){
        res.render('cargaperfil.html', {
            title: "Carga",
            errors,
            nombre, 
            dni,
            email,
            telefono,
            genero
        })
        
    }
    else{*/
        const newUser = new User({nombre, dni, email, telefono, genero});
        await newUser.save();
        console.log(newUser._id)
        res.render('academica.html',{title: 'academica', user: newUser._id})
   // }
    
  });


//Eliminar
router.get( '/delete/:id', async (req, res) => {
    const {id} = req.params;
    await User.remove({_id: id});
    res.redirect('/lista');
})
//Editar
router.get( '/edit/:id', async (req, res) => {
    const {id} = req.params;
    const user = await User.findById(id);
    res.render('editar.html', {
        user,
        title: 'Edicion de Perfil'
       
    })
})

router.post('/edit/:id', async (req,res) => {
    const {id} = req.params;
    await User.update({_id: id}, req.body);
    res.redirect('/lista');
})

//Ver 
router.get( '/detalle/:id', async (req, res) => {
    const {id} = req.params;
    const user = await User.findById(id);
    res.render('detalle.html', {
        user,
        title: 'Vista Completa de Perfil'
       
    })
})

router.get('/detalle', async (req,res) => { 
    const users = await User.find();
    res.render('detalle.html', {
        title: 'Detalle del Perfil',
        users
    })
})


 
module.exports = router;