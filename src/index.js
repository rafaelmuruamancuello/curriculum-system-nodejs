const express = require("express");
const app = express(); //al llamar a express nos devuelve un objeto 
const path = require('path')
const morgan = require ('morgan');
const mongoose = require('mongoose');
const bodyParser = require('body-parser')

//Parsear el body usando body parser
app.use(bodyParser.json()); // body en formato json
app.use(bodyParser.urlencoded({ extended: false })); //body formulario

//Connecting DB
mongoose.connect('mongodb+srv://rafael:Vanguardia_1@cluster0.siyen.mongodb.net/cv-nodejs-mongobd?retryWrites=true&w=majority', { useUnifiedTopology: true , useNewUrlParser: true })
//mongoose.connect('mongodb://localhost/crud-mongo', { useUnifiedTopology: true , useNewUrlParser: true })
    .then(db => console.log('DB Connected'))
    .catch(err => console.log(err));

//Settings
app.set('port', process.env.PORT || 3000); //Indicamos numero de puerto donde correra nuestra app
app.set('view engine', 'ejs'); //Indicamos que usaremos como motor de busqueda ejs 
app.set('views', path.join(__dirname,'/views')) //Url de la carpeta de las views 
app.engine('html', require('ejs').renderFile) //Para poder usar archivos .html, cuando utilizamos el motor de plantilla ejs

//Routes
app.use(require('./routes/routes')); //Importamos las rutas de las pages

//Middlewares
app.use(morgan('dev')); //Muestra peticiones hacia el servidor 




//Static Files
app.use(express.static(path.join(__dirname,'public'))) //Indicamos carpeta de los archivos estaticos



//Run Server
app.listen(app.get('port'), () => { 
    console.log('Server on port', app.get('port')) 
}) 
